window.addEventListener('DOMContentLoaded', () => {

    const icons = document.querySelectorAll('.icon-password');
    const inputs = document.querySelectorAll("input[type='password']");
    const form = document.querySelector('.password-form');

    const errorMessage = document.createElement('div');
    errorMessage.style.cssText = `
        margin-bottom: 24px;
        color: red;
        display: none;
    `;
    errorMessage.textContent = 'Потрібно ввести однакові значення';
    inputs[1].after(errorMessage);
 
    icons.forEach((icon, index) => {
        icon.addEventListener('click', () => {
            const input = inputs[index];
            if (input.type === 'password') {
                input.type = 'text';
                icon.classList.remove('fa-eye');
                icon.classList.add('fa-eye-slash');
            } else {
                input.type = 'password';
                icon.classList.add('fa-eye');
                icon.classList.remove('fa-eye-slash');
            }
        });
    });

    form.addEventListener('submit', (event) => {
        event.preventDefault();

        const firstPassword = inputs[0].value;
        const secondPassword = inputs[1].value;

        if (firstPassword === secondPassword) {
            alert('You are welcome');
            errorMessage.style.display = 'none';
        } else {
            errorMessage.style.display = 'block';
        }
    });
});















